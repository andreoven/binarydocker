FROM node:12.2.0-alpine

WORKDIR /app
COPY . /app

COPY package.json /app/package.json
RUN npm install

CMD ["npm", "start"]